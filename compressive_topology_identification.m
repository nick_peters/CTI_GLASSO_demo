clear all
close all

s = tf('s');

P = 5 %total nr. of nodes
M = 1000 % nr. of samples
m = 10 %order of FIR filters
N = P*m % nr. of nodes times FIR order

% Numerator and denominator of all transfer functions
x11n = -1.5; % numerator of x^1_1
x11d = [3 1]; % denominator
x11 = tf(x11n, x11d); % putting them together to make the transfer function

x23n = 1;
x23d = [3 1 1];
x23 = tf(x23n, x23d);

x32n = -1;
x32d = [2.5 1];
x32 = tf(x32n, x32d);

x34n = 3;
x34d = [2 1.8 3];
x34 = tf(x34n, x34d);

x51n = -2;
x51d = [2 1.8 3];
x51 = tf(x51n, x51d);


% Find the bandwidth of all transfer functions and put them in an array
BW = arrayfun(@bandwidth, [x11, x23, x32, x34, x51]);

% Sample time in Hz at 3 times the highest bandwidth
Ts = max(BW)*3/(2*pi)


%% Run Simulink model
sim('connected_system.slx',Ts*M);

%% Construct A (toeplitz) matrix and a, u and b vectors

% concatinate the input and output vectors
a = [a1, a2, a3, a4, a5];
u = [u1, u2, u3, u4, u5];

b = a - u;
A = [];

 for i = 1:P
   Aj = toeplitz(a(:,i), zeros(1,m));
   A = [A, Aj];
 end

% Use standard matlab solver to find min x ||b - Ax||
% this can be used to verify the CVX optimization results
% since the CVX syntax is a lot more involved
x_ls = A\b;

%% Apply CVX solver to ||b-Ax|| minimization for x
% this should give the same result as the standard matlab solver

x_min = []

% Solve the minimization for each node 
% independently using the same A matrix 
for i=1:P   
    cvx_begin quiet % supress solver output
        variable x(P*m)
        minimize( power(2,norm(A*x-b(:,i))) )
    cvx_end
    
    % Append result to that of previous nodes
    x_min = [x_min', x']';
end

% reshape vecor into P^2 blocks of m samples
x_sep = reshape(x_min,m,P^2);
% calculate power of FIR block
x_min_power = sum(x_sep.^2)/m;


%% Apply LASSO algorithm to make x more sparse

lambda = 5 % weigth term to promote sparsity
x_lasso = []

for i=1:P   
    cvx_begin quiet
        variable x(P*m)
        minimize( power(2,norm(A*x-b(:,i))) + lambda*norm(x,1) );
    cvx_end
    x_lasso = [x_lasso', x']';
end

% reshape vecor into P^2 blocks of m samples
x_sep = reshape(x_lasso,m,P^2);
% calculate power of FIR block
x_lasso_power = sum(x_sep.^2)/m;

%% Apply GLASSO

lambda = 5 % weigth term to promote sparsity
x_glasso = []

for i=1:P   
    cvx_begin quiet
        variable x(P*m)
        expression epsilon(P)
        for j = 0:(P-1)
            epsilon(j+1) = norm(x((j*m+1):((j+1)*m)),2);
        end
        minimize( power(2,norm(A*x-b(:,i))) + lambda*sum(sqrt(m)*epsilon));
    cvx_end
    x_glasso = [x_glasso', x']';
end

% reshape vecor into P^2 blocks of m samples
x_sep = reshape(x_glasso,m,P^2);
% calculate power of FIR block
x_glasso_power = sum(x_sep.^2)/m;


%% Plot resulting FIR arrays for all methods
stem_plot = figure(1);
hold on
stem(x_min);
stem(x_lasso);
stem(x_glasso);

% Draw vertical lines to seperate FIR blocks
% vline function available at the following url
% https://mathworks.com/matlabcentral/fileexchange/1039-hline-and-vline
for i=1:P
    for j=1:P
        index = (i-1)*P + j ;
        vline(index*m,'k:')
    end
end

%% Bar graph for comparisson


x_all = [x_min_power', x_lasso_power', x_glasso_power'];   % concatenate vectors horizontally
c = A(:);      % flatten the result

% Bar plot of energy of FIR blocks (||x_i||)
bar_plot = figure(2)
bar(x_all)

